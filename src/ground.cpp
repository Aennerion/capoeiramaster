#include "ground.h"

#include <QVector2D>
#include <QVector3D>


struct VertexData
{
    QVector3D position;
    QVector3D color;
};


Ground::Ground(): indexBuf(QOpenGLBuffer::IndexBuffer)
{
        initializeOpenGLFunctions();

        // Generate 2 VBOs
        arrayBuf.create();
        indexBuf.create();

        // Initializes cube geometry and transfers it to VBOs
        initGround();

}
void Ground::initGround(){



    VertexData vertices[] = {
        {QVector3D(-100.0f, 0.0f, 100.0f), QVector3D(1.0f, 1.0f,1.0f)},
        {QVector3D(100.0f, 0.0f, 100.0f), QVector3D(1.0f, 1.0f,1.0f)},
        {QVector3D(100.0f, 0.0f, -100.0f), QVector3D(1.0f, 1.0f,1.0f)},

        {QVector3D(100.0f, 0.0f, -100.0f), QVector3D(1.0f, 1.0f,1.0f)},
        {QVector3D(-100.0f, 0.0f, -100.0f), QVector3D(1.0f, 1.0f,1.0f)},
        {QVector3D(-100.0f, 0.0f, 100.0f), QVector3D(1.0f, 1.0f,1.0f)},
    };

    GLushort indices[] = {
        0,1,2,
        3,4,5
    };

   arrayBuf.bind();
   arrayBuf.allocate(vertices, nbrVertices * sizeof(VertexData));

   // Transfer index data to VBO 1
   indexBuf.bind();
   indexBuf.allocate(indices, nbrIndices * sizeof(GLushort));

}


Ground::~Ground()
{
   arrayBuf.destroy();
   indexBuf.destroy();
}


void Ground::drawGround(QOpenGLShaderProgram *program){

    // Tell OpenGL which VBOs to use
    arrayBuf.bind();
    indexBuf.bind();


    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int colorLocation = program->attributeLocation("color");
    program->enableAttributeArray(colorLocation);
    program->setAttributeBuffer(colorLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Draw cube geometry using indices from VBO 1
    glDrawElements(GL_TRIANGLES,3*2,GL_UNSIGNED_SHORT,0);
}
