#include "camera.h"
#include <math.h>

Camera::Camera()
{

}

Camera::~Camera(){

}

void Camera::initCamera(QVector3D position, QVector3D target, QVector3D up){
    this->position = QQuaternion(0.0f,position);
    this->target = QQuaternion(0.0f,target);
    this->up = QQuaternion(1.0f,up);
    this->right = QQuaternion(1.0f,1.0f,0.0f,0.0f);
    this->WorldUp = QQuaternion(0.0f,0.0f,0.0f,0.0f);
    this->delta = 0.1f;
    this->MovementSpeed= 2.5f;
}

void Camera::translatePos(QVector3D translate){
    QQuaternion trans = QQuaternion(0,translate);
    this->position+=trans;
}

QMatrix4x4 Camera::lookAt(){
    QMatrix4x4 matrix = QMatrix4x4();
    matrix.setToIdentity();
    matrix.lookAt(this->position.vector(),this->position.vector() + this->target.vector(),this->up.vector());
    return matrix;
}

QMatrix4x4 Camera::projectionMatrix(int w,int h){
    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 1.0, zFar = 100.0, fov = 45.0;

    QMatrix4x4 matrix = QMatrix4x4();
    matrix.setToIdentity();
    matrix.perspective(fov,aspect,zNear,zFar);
    return matrix;
}

void Camera::changeTarget(QVector3D translate){
    QQuaternion trans = QQuaternion(0,translate);
    this->target+=trans;
}

void Camera::ProcessKeyboard(Camera_Movement direction)
    {
        float velocity = this->MovementSpeed* this->delta;
        if (direction == FORWARD)

            this->translatePos(-QVector3D::crossProduct(this->right.vector(),this->up.vector()).normalized()*velocity);//QVector3D(0.0f,0.0f,-1.0f)
        if (direction == BACKWARD)
            this->translatePos(QVector3D::crossProduct(this->right.vector(),this->up.vector()).normalized()*velocity);//QVector3D(0.0f,0.0f,1.0f)
        if (direction == LEFT)
            this->translatePos(-this->right.vector().normalized()*velocity);//QVector3D(-1.0f,0.0f,0.0f)
        if (direction == RIGHT)
            this->translatePos(this->right.vector().normalized()*velocity);//QVector3D(1.0f,0.0f,0.0f)
        if (direction == UP)
            this->translatePos(this->up.vector().normalized()*velocity);//QVector3D(0.0f,1.0f,0.0f)
        if (direction == DOWN)
            this->translatePos(-this->up.vector().normalized()*velocity);//QVector3D(0.0f,-1.0f,0.0f)
        if (direction == RRIGHT){
            this->rotation(-5.0f,1);
        }

        if (direction == RLEFT){
            this->rotation(5.0f,1);
        }

        if (direction == RUP){
            this->rotation(-5.0f,2);
        }

        if (direction == RDOWN){
            this->rotation(5.0f,2);
        }
}

void Camera::rotation(float angle, int axis){
    float rad =angle * M_PI/180;
    QVector3D axe;
    QQuaternion rotation;
    if (axis ==1){//touche Q et D axe UP
        axe = QVector3D(this->up.vector()).normalized();
        rotation  = QQuaternion(cos(rad/2),axe*sin(rad/2));
        this->target = QQuaternion(rotation.rotatedVector(this->target.vector()));
        this->right = QQuaternion(rotation.rotatedVector(this->right.vector()));

    }
    else if(axis == 2){//touche Z et S axe Right
        axe = QVector3D(this->right.vector()).normalized();
        rotation  = QQuaternion(cos(rad/2),axe*sin(rad/2));
        this->target = QQuaternion(rotation.rotatedVector(this->target.vector()));
        this->up = QQuaternion(rotation.rotatedVector(this->up.vector()));
        }
    else {
        return;
    }

}
