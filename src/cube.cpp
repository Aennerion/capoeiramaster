#include "cube.h"

#include <QVector2D>
#include <QVector3D>


struct VertexData
{
    QVector3D position;
    QVector3D color;
};

Cube::Cube(): indexBuf(QOpenGLBuffer::IndexBuffer)
{
        initializeOpenGLFunctions();

        // Generate 2 VBOs
        arrayBuf.create();
        indexBuf.create();

        // Initializes cube geometry and transfers it to VBOs
        initCube();

}
 void Cube::initCube(){



     VertexData vertices[] = {
         {QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.0f, 0.0f,1.0f)},
         {QVector3D(1.0f, 0.0f, 0.0f), QVector3D(0.0f, 0.0f,1.0f)},
         {QVector3D(1.0f, 0.0f, 1.0f), QVector3D(0.0f, 0.0f,1.0f)},


         {QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.0f, 0.0f,1.0f)},
         {QVector3D(1.0f, 0.0f, 1.0f), QVector3D(0.0f, 0.0f,1.0f)},
         {QVector3D(0.0f, 0.0f, 1.0f), QVector3D(0.0f, 0.0f,1.0f)},


         {QVector3D(0.0f, 0.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},
         {QVector3D(1.0f, 0.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},


         {QVector3D(1.0f, 0.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},
         {QVector3D(1.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 0.0f)},


         {QVector3D(0.0f, 0.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(1.0f, 0.0f, 0.0f)},
         {QVector3D(0.0f, 0.0f, 0.0f), QVector3D(1.0f, 0.0f, 0.0f)},

         {QVector3D(0.0f, 0.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(1.0f, 0.0f, 0.0f)},

         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},
         {QVector3D(1.0f, 0.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},
         {QVector3D(0.0f, 0.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},

         {QVector3D(1.0f, 0.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},
         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},
         {QVector3D(1.0f, 1.0f, 0.0f), QVector3D(1.0f, 1.0f, 0.0f)},


         {QVector3D(1.0f, 1.0f, 0.0f), QVector3D(1.0f, 0.0f, 1.0f)},
         {QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 0.0f, 1.0f)},
         {QVector3D(1.0f, 0.0f, 0.0f), QVector3D(1.0f, 0.0f, 1.0f)},

         {QVector3D(1.0f, 0.0f, 0.0f), QVector3D(1.0f, 0.0f, 1.0f)},
         {QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 0.0f, 1.0f)},
         {QVector3D(1.0f, 0.0f, 1.0f), QVector3D(1.0f, 0.0f, 1.0f)},

         {QVector3D(1.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 1.0f)},
         {QVector3D(1.0f, 1.0f, 0.0f), QVector3D(0.0f, 1.0f, 1.0f)},
         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(0.0f, 1.0f, 1.0f)},


         {QVector3D(1.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 1.0f)},
         {QVector3D(0.0f, 1.0f, 0.0f), QVector3D(0.0f, 1.0f, 1.0f)},
         {QVector3D(0.0f, 1.0f, 1.0f), QVector3D(0.0f, 1.0f, 1.0f)},

     };

     GLushort indices[] = {
         0,1,2,
         3,4,5,
         6,7,8,
         9,10,11,
         12,13,14,
         15,16,17,
         18,19,20,
         21,22,23,
         24,25,26,
         27,28,29,
         30,31,32,
         33,34,35,
     };

    arrayBuf.bind();
    arrayBuf.allocate(vertices, nbrVertices * sizeof(VertexData));

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices, nbrIndices * sizeof(GLushort));

}


Cube::~Cube()
{
    arrayBuf.destroy();
    indexBuf.destroy();
}

void Cube::drawCube(QOpenGLShaderProgram *program){

    // Tell OpenGL which VBOs to use
    arrayBuf.bind();
    indexBuf.bind();


    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int colorLocation = program->attributeLocation("color");
    program->enableAttributeArray(colorLocation);
    program->setAttributeBuffer(colorLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Draw cube geometry using indices from VBO 1
    glDrawElements(GL_TRIANGLES,3*12,GL_UNSIGNED_SHORT,0);
}

