#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include "cube.h"
#include "camera.h"
#include "ground.h"
#include "personnageloader.h"

class mainwidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    explicit mainwidget(QWidget *parent = 0);
    ~mainwidget();
protected:

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void initShaders();
    void initTextures();
    void keyPressEvent(QKeyEvent *event) override;

private:
    QBasicTimer timer;
    QOpenGLShaderProgram program;
    Cube *cube;
    Camera* camera;
    Ground* ground;

    QMatrix4x4 projection;

    QVector2D mousePressPosition;
    QVector3D rotationAxis;
    qreal angularSpeed;
    QQuaternion rotation;
    QMatrix4x4 vue;
    PersonnageLoader personnage;


};

#endif // MAINWIDGET_H
