#ifndef CAMERA_H
#define CAMERA_H

#include <QQuaternion>
#include <QMatrix4x4>
#include <QVector3D>


enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    RRIGHT,
    RLEFT,
    RUP,
    RDOWN
};


class Camera
{
public:
    Camera();
    ~Camera();

    void initCamera(QVector3D position = QVector3D(0.0f,0.0f,0.0f), QVector3D target = QVector3D(0.0f,0.0f,-1.0f), QVector3D up = QVector3D(0.0f,1.0f,0.0f));
    void translatePos(QVector3D translate);
    QMatrix4x4 lookAt();
    QMatrix4x4 projectionMatrix(int w,int h);
    void changeTarget(QVector3D translate);
    void ProcessKeyboard(Camera_Movement direction);
    void rotation(float angle,int axis);


private:
    QQuaternion position;
    QQuaternion target;
    QQuaternion up;
    QQuaternion right;
    QQuaternion WorldUp;
    float delta;
    float MovementSpeed;
};

#endif // CAMERA_H
