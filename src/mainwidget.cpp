#include "mainwidget.h"

#include <QMouseEvent>

#include <math.h>




mainwidget::mainwidget(QWidget *parent):QOpenGLWidget(parent),angularSpeed(0)
{

}
mainwidget::~mainwidget()
{
    // Make sure the context is current when deleting the texture
    // and the buffers.
    makeCurrent();
    delete cube;
    doneCurrent();
}

void mainwidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0, 0, 0, 1);

    initShaders();


    // Enable depth buffer
    glEnable(GL_DEPTH_TEST);

    // Enable back face culling
    glEnable(GL_CULL_FACE);


    cube = new Cube;
    ground = new Ground();

    //commenter car du jours au lendemant j'ai un erreur de linking ma lib de cahrgement des .dae
    //personnage = PersonnageLoader("../ressources/base_model_dab.dae");
    //personnage.program = &this->program;

    camera = new Camera;
    camera->initCamera(QVector3D(0.0f,2.0f,-8.0f),QVector3D(0.0f,0.0f,1.0f),QVector3D(0.0f,1.0f,0.0f));
    vue = camera->lookAt();


}

void mainwidget::initShaders()
{
    // Compile vertex shader
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vshader.glsl"))
        close();

    // Compile fragment shader
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fshader.glsl"))
        close();

    // Link shader pipeline
    if (!program.link())
        close();

    // Bind shader pipeline for use
    if (!program.bind())
        close();
}

void mainwidget::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
        {

            case Qt::Key_Left:
                qInfo("Left");
                camera->ProcessKeyboard(Camera_Movement::RIGHT);
                break;

            case Qt::Key_Right:
                qInfo("Right");
                camera->ProcessKeyboard(Camera_Movement::LEFT);
                break;

            case Qt::Key_Down:
                        qInfo("Down");
                camera->ProcessKeyboard(Camera_Movement::FORWARD);
                break;

            case Qt::Key_Up:
                        qInfo("UP");
                camera->ProcessKeyboard(Camera_Movement::BACKWARD);
                break;
            case Qt::Key_Space:
                        qInfo("Space");
                camera->ProcessKeyboard(Camera_Movement::UP);
            break;
            case Qt::Key_Control:
                                qInfo("CTRL");
                camera->ProcessKeyboard(Camera_Movement::DOWN);
                break;
            case Qt::Key_Z:
                                qInfo("Z");
                camera->ProcessKeyboard(Camera_Movement::RUP);
                break;
            case Qt::Key_S:
                                qInfo("S");
                camera->ProcessKeyboard(Camera_Movement::RDOWN);
                break;
            case Qt::Key_Q:
                                qInfo("Q");
                camera->ProcessKeyboard(Camera_Movement::RLEFT);
                break;
            case Qt::Key_D:
                                qInfo("D");
                camera->ProcessKeyboard(Camera_Movement::RRIGHT);
                break;
        }
        update();
}

void mainwidget::resizeGL(int w, int h)
{
    this->projection = camera->projectionMatrix(w,h);
}

void mainwidget::paintGL()
{
    vue = camera->lookAt();
    //glm::vec3(100.0f, 1.0f, 100.0f) //ground
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 matrix;
    matrix.translate(0.0,0.0,0.0);
    program.setUniformValue("mvp", projection * vue * matrix);
    ground->drawGround(&program);


    matrix.translate(0.0, 0.5, 0.0);
    //matrix.rotate(rotation);
    program.setUniformValue("mvp", projection  * vue * matrix);
    cube->drawCube(&program);

    /*
    //affiche normalement le personnge
    program.setUniformValue("mvp", projection * vue * matrix);
    //Pop();
    personnage.Draw(&program);
    */


}
