#ifndef GROUND_H
#define GROUND_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>


class Ground : protected QOpenGLFunctions
{
public:
    Ground();
    virtual ~Ground();
    void    drawGround(QOpenGLShaderProgram *program);

private:
     void initGround();

    QOpenGLBuffer arrayBuf;
    QOpenGLBuffer indexBuf;

    const int nbrVertices = 3*2;

    const int nbrIndices = 3*2;

};

#endif // GROUND_H
