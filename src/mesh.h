

#ifndef MESH_H
#define MESH_H

#include <lib_externes/glm/glm.hpp>
#include <lib_externes/glm/gtc/matrix_transform.hpp>
/*
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
*/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>


using namespace std;

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture {
    unsigned int id;
    string type;
    string path;
};

class Mesh: protected QOpenGLFunctions
{
public:
    /*  Mesh Data  */
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    vector<Texture> textures;

    /*  Functions  */
    // constructor
    Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures,QOpenGLShaderProgram *program)
    {
        this->vertices = vertices;
        this->indices = indices;
        this->textures = textures;

        // now that we have all the required data, set the vertex buffers and its attribute pointers.
        setupMesh(program);
    }

    // render the mesh
    void Draw(QOpenGLShaderProgram *program)
    {
        VBO.bind();
        EBO.bind();
        // bind appropriate textures
        unsigned int diffuseNr  = 1;
        unsigned int specularNr = 1;
        unsigned int normalNr   = 1;
        unsigned int heightNr   = 1;
        for(unsigned int i = 0; i < textures.size(); i++)
        {
            glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
            // retrieve texture number (the N in diffuse_textureN)
            string number;
            string name = textures[i].type;
            if(name == "texture_diffuse")
                number = std::to_string(diffuseNr++);
            else if(name == "texture_specular")
                number = std::to_string(specularNr++); // transfer unsigned int to stream
            else if(name == "texture_normal")
                number = std::to_string(normalNr++); // transfer unsigned int to stream
             else if(name == "texture_height")
                number = std::to_string(heightNr++); // transfer unsigned int to stream

                                                     // now set the sampler to the correct texture unit
            int colorLocation = program->attributeLocation("color");
            glUniform1i(glGetUniformLocation(colorLocation, (name + number).c_str()), i);
            // and finally bind the texture
            glBindTexture(GL_TEXTURE_2D, textures[i].id);
        }

        // draw mesh

        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

        // always good practice to set everything back to defaults once configured.
        glActiveTexture(GL_TEXTURE0);
        VBO.release();
        EBO.release();
    }

private:
    /*  Render data  */
    QOpenGLBuffer VBO, EBO;

    /*  Functions    */
    // initializes all the buffer objects/arrays
    void setupMesh(QOpenGLShaderProgram *program)
    {
        // create buffers/arrays
        VBO.create();
        EBO.create();


        // load data into vertex buffers
        VBO.bind();

        // A great thing about structs is that their memory layout is sequential for all its items.
        // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
        // again translates to 3/2 floats which translates to a byte array.
        Vertex* v=&this->vertices[0];
        VBO.allocate(v,vertices.size() * sizeof(Vertex));

        EBO.bind();
        unsigned int* i=&this->indices[0];
        EBO.allocate(i,indices.size() * sizeof(unsigned int));

        // set the vertex attribute pointers
        // vertex Positions
        int vertexLocation = program->attributeLocation("position");
        program->enableAttributeArray(vertexLocation);
        program->setAttributeBuffer(vertexLocation,GL_FLOAT,0,sizeof(Vertex));
        /*
        // vertex normals
        int normalLocation = program->attributeLocation("normale");
        program->enableAttributeArray(normalLocation);
        program->setAttributeBuffer(normalLocation,GL_FLOAT,offsetof(Vertex, Normal),sizeof(Vertex));
        // vertex texture coords
        int textureLocation = program->attributeLocation("texture");
        program->enableAttributeArray(textureLocation);
        program->setAttributeBuffer(textureLocation,GL_FLOAT,offsetof(Vertex, TexCoords),sizeof(Vertex));
        // vertex tangent
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
        int tangentLocation = program->attributeLocation("tangent");
        program->enableAttributeArray(tangentLocation);
        program->setAttributeBuffer(tangentLocation,GL_FLOAT,offsetof(Vertex, Tangent),sizeof(Vertex));
        // vertex bitangent
        int bitangentLocation = program->attributeLocation("bitangent");
        program->enableAttributeArray(bitangentLocation);
        program->setAttributeBuffer(bitangentLocation,GL_FLOAT,offsetof(Vertex, Bitangent),sizeof(Vertex));*/
        VBO.release();
        EBO.release();
    }
};
#endif

