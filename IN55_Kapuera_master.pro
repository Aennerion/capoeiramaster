#-------------------------------------------------
#
# Project created by QtCreator 2019-05-27T17:49:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IN55_Kapuera_master
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    lib_externes/glm/detail/glm.cpp \
    src/personnageloader.cpp \
    src/camera.cpp \
    src/cube.cpp \
    src/ground.cpp \
        src/main.cpp \
        src/mainwidget.cpp \

HEADERS += \
    lib_externes/glm/common.hpp \
    lib_externes/glm/detail/_features.hpp \
    lib_externes/glm/detail/_fixes.hpp \
    lib_externes/glm/detail/_noise.hpp \
    lib_externes/glm/detail/_swizzle.hpp \
    lib_externes/glm/detail/_swizzle_func.hpp \
    lib_externes/glm/detail/_vectorize.hpp \
    lib_externes/glm/detail/compute_common.hpp \
    lib_externes/glm/detail/compute_vector_relational.hpp \
    lib_externes/glm/detail/qualifier.hpp \
    lib_externes/glm/detail/setup.hpp \
    lib_externes/glm/detail/type_float.hpp \
    lib_externes/glm/detail/type_half.hpp \
    lib_externes/glm/detail/type_mat2x2.hpp \
    lib_externes/glm/detail/type_mat2x3.hpp \
    lib_externes/glm/detail/type_mat2x4.hpp \
    lib_externes/glm/detail/type_mat3x2.hpp \
    lib_externes/glm/detail/type_mat3x3.hpp \
    lib_externes/glm/detail/type_mat3x4.hpp \
    lib_externes/glm/detail/type_mat4x2.hpp \
    lib_externes/glm/detail/type_mat4x3.hpp \
    lib_externes/glm/detail/type_mat4x4.hpp \
    lib_externes/glm/detail/type_quat.hpp \
    lib_externes/glm/detail/type_vec1.hpp \
    lib_externes/glm/detail/type_vec2.hpp \
    lib_externes/glm/detail/type_vec3.hpp \
    lib_externes/glm/detail/type_vec4.hpp \
    lib_externes/glm/exponential.hpp \
    lib_externes/glm/ext.hpp \
    lib_externes/glm/ext/matrix_clip_space.hpp \
    lib_externes/glm/ext/matrix_common.hpp \
    lib_externes/glm/ext/matrix_double2x2.hpp \
    lib_externes/glm/ext/matrix_double2x2_precision.hpp \
    lib_externes/glm/ext/matrix_double2x3.hpp \
    lib_externes/glm/ext/matrix_double2x3_precision.hpp \
    lib_externes/glm/ext/matrix_double2x4.hpp \
    lib_externes/glm/ext/matrix_double2x4_precision.hpp \
    lib_externes/glm/ext/matrix_double3x2.hpp \
    lib_externes/glm/ext/matrix_double3x2_precision.hpp \
    lib_externes/glm/ext/matrix_double3x3.hpp \
    lib_externes/glm/ext/matrix_double3x3_precision.hpp \
    lib_externes/glm/ext/matrix_double3x4.hpp \
    lib_externes/glm/ext/matrix_double3x4_precision.hpp \
    lib_externes/glm/ext/matrix_double4x2.hpp \
    lib_externes/glm/ext/matrix_double4x2_precision.hpp \
    lib_externes/glm/ext/matrix_double4x3.hpp \
    lib_externes/glm/ext/matrix_double4x3_precision.hpp \
    lib_externes/glm/ext/matrix_double4x4.hpp \
    lib_externes/glm/ext/matrix_double4x4_precision.hpp \
    lib_externes/glm/ext/matrix_float2x2.hpp \
    lib_externes/glm/ext/matrix_float2x2_precision.hpp \
    lib_externes/glm/ext/matrix_float2x3.hpp \
    lib_externes/glm/ext/matrix_float2x3_precision.hpp \
    lib_externes/glm/ext/matrix_float2x4.hpp \
    lib_externes/glm/ext/matrix_float2x4_precision.hpp \
    lib_externes/glm/ext/matrix_float3x2.hpp \
    lib_externes/glm/ext/matrix_float3x2_precision.hpp \
    lib_externes/glm/ext/matrix_float3x3.hpp \
    lib_externes/glm/ext/matrix_float3x3_precision.hpp \
    lib_externes/glm/ext/matrix_float3x4.hpp \
    lib_externes/glm/ext/matrix_float3x4_precision.hpp \
    lib_externes/glm/ext/matrix_float4x2.hpp \
    lib_externes/glm/ext/matrix_float4x2_precision.hpp \
    lib_externes/glm/ext/matrix_float4x3.hpp \
    lib_externes/glm/ext/matrix_float4x3_precision.hpp \
    lib_externes/glm/ext/matrix_float4x4.hpp \
    lib_externes/glm/ext/matrix_float4x4_precision.hpp \
    lib_externes/glm/ext/matrix_projection.hpp \
    lib_externes/glm/ext/matrix_relational.hpp \
    lib_externes/glm/ext/matrix_transform.hpp \
    lib_externes/glm/ext/quaternion_common.hpp \
    lib_externes/glm/ext/quaternion_double.hpp \
    lib_externes/glm/ext/quaternion_double_precision.hpp \
    lib_externes/glm/ext/quaternion_exponential.hpp \
    lib_externes/glm/ext/quaternion_float.hpp \
    lib_externes/glm/ext/quaternion_float_precision.hpp \
    lib_externes/glm/ext/quaternion_geometric.hpp \
    lib_externes/glm/ext/quaternion_relational.hpp \
    lib_externes/glm/ext/quaternion_transform.hpp \
    lib_externes/glm/ext/quaternion_trigonometric.hpp \
    lib_externes/glm/ext/scalar_common.hpp \
    lib_externes/glm/ext/scalar_constants.hpp \
    lib_externes/glm/ext/scalar_int_sized.hpp \
    lib_externes/glm/ext/scalar_relational.hpp \
    lib_externes/glm/ext/scalar_uint_sized.hpp \
    lib_externes/glm/ext/scalar_ulp.hpp \
    lib_externes/glm/ext/vector_bool1.hpp \
    lib_externes/glm/ext/vector_bool1_precision.hpp \
    lib_externes/glm/ext/vector_bool2.hpp \
    lib_externes/glm/ext/vector_bool2_precision.hpp \
    lib_externes/glm/ext/vector_bool3.hpp \
    lib_externes/glm/ext/vector_bool3_precision.hpp \
    lib_externes/glm/ext/vector_bool4.hpp \
    lib_externes/glm/ext/vector_bool4_precision.hpp \
    lib_externes/glm/ext/vector_common.hpp \
    lib_externes/glm/ext/vector_double1.hpp \
    lib_externes/glm/ext/vector_double1_precision.hpp \
    lib_externes/glm/ext/vector_double2.hpp \
    lib_externes/glm/ext/vector_double2_precision.hpp \
    lib_externes/glm/ext/vector_double3.hpp \
    lib_externes/glm/ext/vector_double3_precision.hpp \
    lib_externes/glm/ext/vector_double4.hpp \
    lib_externes/glm/ext/vector_double4_precision.hpp \
    lib_externes/glm/ext/vector_float1.hpp \
    lib_externes/glm/ext/vector_float1_precision.hpp \
    lib_externes/glm/ext/vector_float2.hpp \
    lib_externes/glm/ext/vector_float2_precision.hpp \
    lib_externes/glm/ext/vector_float3.hpp \
    lib_externes/glm/ext/vector_float3_precision.hpp \
    lib_externes/glm/ext/vector_float4.hpp \
    lib_externes/glm/ext/vector_float4_precision.hpp \
    lib_externes/glm/ext/vector_int1.hpp \
    lib_externes/glm/ext/vector_int1_precision.hpp \
    lib_externes/glm/ext/vector_int2.hpp \
    lib_externes/glm/ext/vector_int2_precision.hpp \
    lib_externes/glm/ext/vector_int3.hpp \
    lib_externes/glm/ext/vector_int3_precision.hpp \
    lib_externes/glm/ext/vector_int4.hpp \
    lib_externes/glm/ext/vector_int4_precision.hpp \
    lib_externes/glm/ext/vector_relational.hpp \
    lib_externes/glm/ext/vector_uint1.hpp \
    lib_externes/glm/ext/vector_uint1_precision.hpp \
    lib_externes/glm/ext/vector_uint2.hpp \
    lib_externes/glm/ext/vector_uint2_precision.hpp \
    lib_externes/glm/ext/vector_uint3.hpp \
    lib_externes/glm/ext/vector_uint3_precision.hpp \
    lib_externes/glm/ext/vector_uint4.hpp \
    lib_externes/glm/ext/vector_uint4_precision.hpp \
    lib_externes/glm/ext/vector_ulp.hpp \
    lib_externes/glm/fwd.hpp \
    lib_externes/glm/geometric.hpp \
    lib_externes/glm/glm.hpp \
    lib_externes/glm/gtc/bitfield.hpp \
    lib_externes/glm/gtc/color_space.hpp \
    lib_externes/glm/gtc/constants.hpp \
    lib_externes/glm/gtc/epsilon.hpp \
    lib_externes/glm/gtc/integer.hpp \
    lib_externes/glm/gtc/matrix_access.hpp \
    lib_externes/glm/gtc/matrix_integer.hpp \
    lib_externes/glm/gtc/matrix_inverse.hpp \
    lib_externes/glm/gtc/matrix_transform.hpp \
    lib_externes/glm/gtc/noise.hpp \
    lib_externes/glm/gtc/packing.hpp \
    lib_externes/glm/gtc/quaternion.hpp \
    lib_externes/glm/gtc/random.hpp \
    lib_externes/glm/gtc/reciprocal.hpp \
    lib_externes/glm/gtc/round.hpp \
    lib_externes/glm/gtc/type_aligned.hpp \
    lib_externes/glm/gtc/type_precision.hpp \
    lib_externes/glm/gtc/type_ptr.hpp \
    lib_externes/glm/gtc/ulp.hpp \
    lib_externes/glm/gtc/vec1.hpp \
    lib_externes/glm/gtx/associated_min_max.hpp \
    lib_externes/glm/gtx/bit.hpp \
    lib_externes/glm/gtx/closest_point.hpp \
    lib_externes/glm/gtx/color_encoding.hpp \
    lib_externes/glm/gtx/color_space.hpp \
    lib_externes/glm/gtx/color_space_YCoCg.hpp \
    lib_externes/glm/gtx/common.hpp \
    lib_externes/glm/gtx/compatibility.hpp \
    lib_externes/glm/gtx/component_wise.hpp \
    lib_externes/glm/gtx/dual_quaternion.hpp \
    lib_externes/glm/gtx/easing.hpp \
    lib_externes/glm/gtx/euler_angles.hpp \
    lib_externes/glm/gtx/extend.hpp \
    lib_externes/glm/gtx/extended_min_max.hpp \
    lib_externes/glm/gtx/exterior_product.hpp \
    lib_externes/glm/gtx/fast_exponential.hpp \
    lib_externes/glm/gtx/fast_square_root.hpp \
    lib_externes/glm/gtx/fast_trigonometry.hpp \
    lib_externes/glm/gtx/functions.hpp \
    lib_externes/glm/gtx/gradient_paint.hpp \
    lib_externes/glm/gtx/handed_coordinate_space.hpp \
    lib_externes/glm/gtx/hash.hpp \
    lib_externes/glm/gtx/integer.hpp \
    lib_externes/glm/gtx/intersect.hpp \
    lib_externes/glm/gtx/io.hpp \
    lib_externes/glm/gtx/log_base.hpp \
    lib_externes/glm/gtx/matrix_cross_product.hpp \
    lib_externes/glm/gtx/matrix_decompose.hpp \
    lib_externes/glm/gtx/matrix_factorisation.hpp \
    lib_externes/glm/gtx/matrix_interpolation.hpp \
    lib_externes/glm/gtx/matrix_major_storage.hpp \
    lib_externes/glm/gtx/matrix_operation.hpp \
    lib_externes/glm/gtx/matrix_query.hpp \
    lib_externes/glm/gtx/matrix_transform_2d.hpp \
    lib_externes/glm/gtx/mixed_product.hpp \
    lib_externes/glm/gtx/norm.hpp \
    lib_externes/glm/gtx/normal.hpp \
    lib_externes/glm/gtx/normalize_dot.hpp \
    lib_externes/glm/gtx/number_precision.hpp \
    lib_externes/glm/gtx/optimum_pow.hpp \
    lib_externes/glm/gtx/orthonormalize.hpp \
    lib_externes/glm/gtx/perpendicular.hpp \
    lib_externes/glm/gtx/polar_coordinates.hpp \
    lib_externes/glm/gtx/projection.hpp \
    lib_externes/glm/gtx/quaternion.hpp \
    lib_externes/glm/gtx/range.hpp \
    lib_externes/glm/gtx/raw_data.hpp \
    lib_externes/glm/gtx/rotate_normalized_axis.hpp \
    lib_externes/glm/gtx/rotate_vector.hpp \
    lib_externes/glm/gtx/scalar_multiplication.hpp \
    lib_externes/glm/gtx/scalar_relational.hpp \
    lib_externes/glm/gtx/spline.hpp \
    lib_externes/glm/gtx/std_based_type.hpp \
    lib_externes/glm/gtx/string_cast.hpp \
    lib_externes/glm/gtx/texture.hpp \
    lib_externes/glm/gtx/transform.hpp \
    lib_externes/glm/gtx/transform2.hpp \
    lib_externes/glm/gtx/type_aligned.hpp \
    lib_externes/glm/gtx/type_trait.hpp \
    lib_externes/glm/gtx/vec_swizzle.hpp \
    lib_externes/glm/gtx/vector_angle.hpp \
    lib_externes/glm/gtx/vector_query.hpp \
    lib_externes/glm/gtx/wrap.hpp \
    lib_externes/glm/integer.hpp \
    lib_externes/glm/mat2x2.hpp \
    lib_externes/glm/mat2x3.hpp \
    lib_externes/glm/mat2x4.hpp \
    lib_externes/glm/mat3x2.hpp \
    lib_externes/glm/mat3x3.hpp \
    lib_externes/glm/mat3x4.hpp \
    lib_externes/glm/mat4x2.hpp \
    lib_externes/glm/mat4x3.hpp \
    lib_externes/glm/mat4x4.hpp \
    lib_externes/glm/matrix.hpp \
    lib_externes/glm/packing.hpp \
    lib_externes/glm/simd/common.h \
    lib_externes/glm/simd/exponential.h \
    lib_externes/glm/simd/geometric.h \
    lib_externes/glm/simd/integer.h \
    lib_externes/glm/simd/matrix.h \
    lib_externes/glm/simd/packing.h \
    lib_externes/glm/simd/platform.h \
    lib_externes/glm/simd/trigonometric.h \
    lib_externes/glm/simd/vector_relational.h \
    lib_externes/glm/trigonometric.hpp \
    lib_externes/glm/vec2.hpp \
    lib_externes/glm/vec3.hpp \
    lib_externes/glm/vec4.hpp \
    lib_externes/glm/vector_relational.hpp \
    src/mesh.h \
    src/personnageloader.h \
    src/camera.h \
    src/cube.h \
    src/ground.h \
    src/mainwidget.h \ \
    lib_externes/stb/stb_image.h \

RESOURCES += \
    ressources/shaders.qrc \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib_externes/assimp/lib/ -lassimp-vc142-mtd
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib_externes/assimp/lib/ -lassimp-vc142-mtd
else:unix: LIBS += -L$$PWD/lib_externes/assimp/lib/ -lassimp-vc142-mt

INCLUDEPATH += $$PWD/lib_externes/assimp/include
DEPENDPATH += $$PWD/lib_externes/assimp/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib_externes/glew-2.1.0/lib/Release/x64/ -lglew32
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib_externes/glew-2.1.0/lib/Release/x64/ -lglew32
else:unix: LIBS += -L$$PWD/lib_externes/glew-2.1.0/lib/Release/x64/ -lglew32

INCLUDEPATH += $$PWD/lib_externes/glew-2.1.0/include
DEPENDPATH += $$PWD/lib_externes/glew-2.1.0/include

INCLUDEPATH += $$PWD/lib_externes/stb
DEPENDPATH += $$PWD/lib_externes/stb

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib_externes/glfw/lib/ -lglfw3
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib_externes/glfw/lib/ -lglfw3
else:unix: LIBS += -L$$PWD/lib_externes/glfw/lib/ -lglfw3

INCLUDEPATH += $$PWD/lib_externes/glfw/include
DEPENDPATH += $$PWD/lib_externes/glfw/include
